from django.db.models.fields.files import FieldFile
from django.conf import settings
from rest_framework import serializers
from justwork.celery import app

from redis import StrictRedis

from justwork.models import Page, PageElement, Content, TextContent, \
    AudioContent, VideoContent


class PageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ('id', 'url', 'title')


class ContentSerializer(serializers.ModelSerializer):
    data = serializers.SerializerMethodField()

    class Meta:
        model = Content
        fields = ('id', 'title', 'counter', 'data')

    def get_data(self, obj):
        # try to find nested object and get its properties
        types = {'text': TextContent, 'audio': AudioContent,
                 'video': VideoContent}
        for name, model in types.items():
            real_objs = model.objects.filter(id=obj.id)
            if real_objs:
                # select first found object
                real_obj = real_objs[0]

                # get model fields except content_ptr special field
                fields = [o.name for o in real_obj._meta.local_fields
                          if o.name != 'content_ptr']

                # set content type
                result = {'type': name}

                # read model instance field values
                for field in fields:
                    value = getattr(real_obj, field)
                    # use url for file fields, native value for others
                    result[field] = value.url if isinstance(value, FieldFile) \
                        else value

                # increase counter on request
                # use Celery in production mode
                if settings.DEBUG:
                    self.increase_counter(obj.id)
                else:
                    self.increase_counter.delay(obj.id)

                return result

        # Unknown type? Let's raise error
        raise AttributeError('Unknown type for content. id=%d' % obj.id)

    @staticmethod
    @app.task
    def increase_counter(id_):
        # Use Redis to lock execution
        with StrictRedis(host=settings.REDIS['HOST'],
                         port=settings.REDIS['PORT'],
                         password=settings.REDIS['PASSWORD'])\
                .lock('justwork:counter:content:%d' % id_):
            content = Content.objects.get(id=id_)
            content.counter += 1
            content.save()
        return content.counter


class PageElementSerializer(serializers.ModelSerializer):
    content = ContentSerializer(read_only=True)

    class Meta:
        model = PageElement
        fields = ('content', 'position', 'width', 'height', 'order')


class PageDetailSerializer(serializers.ModelSerializer):
    elements = PageElementSerializer(many=True, read_only=True)

    class Meta:
        model = Page
        fields = ('id', 'title', 'elements')

    def to_representation(self, value):

        result = super(PageDetailSerializer, self).to_representation(value)

        # apply ordering by elements if possible
        ordering = self.context['request'].query_params.get('ordering')
        if ordering:
            elements = result['elements']
            desc = ordering[0] == '-'
            if desc:
                ordering = ordering[1:]

            # use nesting sorting if possible
            elements.sort(key=lambda x: x[ordering] if ordering in x else
                          x[ordering.split('__')[0]][ordering.split('__')[1]],
                          reverse=desc)

        return result

