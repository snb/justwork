# Generated by Django 2.1 on 2018-08-08 12:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('justwork', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='page',
            options={'ordering': ('title',)},
        ),
        migrations.AlterField(
            model_name='pageelement',
            name='content',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='elements', to='justwork.Content'),
        ),
        migrations.AlterField(
            model_name='pageelement',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='elements', to='justwork.Page'),
        ),
    ]
