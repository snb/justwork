from django.contrib import admin
from justwork.models import Content, TextContent, AudioContent, VideoContent, \
    Page, PageElement


class ContentAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    readonly_fields = ('counter',)


class TextContentAdmin(ContentAdmin):
    search_fields = ('title', 'text')


class PageElementInline(admin.TabularInline):
    model = PageElement
    extra = 1


class PageAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    inlines = (PageElementInline,)


admin.site.register(Content, ContentAdmin)
admin.site.register(TextContent, TextContentAdmin)
admin.site.register(AudioContent, ContentAdmin)
admin.site.register(VideoContent, ContentAdmin)
admin.site.register(Page, PageAdmin)
