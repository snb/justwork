from django.db import models


class Content(models.Model):
    title = models.CharField(max_length=200)
    counter = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.title


class TextContent(Content):
    text = models.TextField()


class VideoContent(Content):
    source = models.FileField(upload_to='video/', null=True)
    width = models.PositiveIntegerField(default=0)
    height = models.PositiveIntegerField(default=0)


class AudioContent(Content):
    source = models.FileField(upload_to='audio/', null=True)
    length = models.PositiveIntegerField(default=0)


class Page(models.Model):
    title = models.CharField(max_length=200)

    class Meta:
        ordering = ('title',)

    def __str__(self):
        return self.title


class PageElement(models.Model):
    page = models.ForeignKey(Page, on_delete=models.PROTECT,
                             related_name='elements')
    content = models.ForeignKey(Content, on_delete=models.PROTECT,
                                related_name='elements')
    position = models.PositiveIntegerField(default=0)
    width = models.PositiveIntegerField(default=0)
    height = models.PositiveIntegerField(default=0)
    order = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('order',)
