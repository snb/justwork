from rest_framework import viewsets
from rest_framework import filters

from justwork.models import Page
from justwork.serializers import PageSerializer, PageDetailSerializer


class PageOrdering(filters.OrderingFilter):
    def filter_queryset(self, request, queryset, view):

        params = request.query_params.get(self.ordering_param)

        if params:
            ordering = [param.strip() for param in params.split(',')]
            return queryset.order_by(*ordering)

        return queryset


class PageViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    filter_backends = (PageOrdering,)

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = PageDetailSerializer
        return super(PageViewSet, self).retrieve(request, *args, **kwargs)
