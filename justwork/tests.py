import json
import unittest

from django.conf import settings
from rest_framework.test import RequestsClient

from justwork.models import TextContent, AudioContent, VideoContent, \
    PageElement, Page


class TestApi(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestApi, self).__init__(*args, **kwargs)
        settings.DEBUG = True

    def setUp(self):
        text_1, is_new = TextContent.objects.get_or_create(
            title='My Text 1',
            text='My Text Content 1')
        audio_1, is_new = AudioContent.objects.get_or_create(
            title='My Audio 1',
            source='audio/TheHomeBabies-'
                   'TheState.mp3',
            length=50)
        video_1, is_new = VideoContent.objects.get_or_create(
            title='My Video 1',
            source='video/messi.mp4',
            width=100, height=100)
        video_2, is_new = VideoContent.objects.get_or_create(
            title='My Video 2',
            source='video/ronaldu.mp4',
            width=200, height=200)

        page_1, is_new = Page.objects.get_or_create(title='My Page 1')
        PageElement.objects.get_or_create(page=page_1, content=text_1,
                                          position=0, width=100, height=100,
                                          order=0)
        PageElement.objects.get_or_create(page=page_1, content=audio_1,
                                          position=100, width=50, height=20,
                                          order=1)
        PageElement.objects.get_or_create(page=page_1, content=video_1,
                                          position=0, width=200, height=200,
                                          order=2)

        page_2, is_new = Page.objects.get_or_create(title='My Page 2')
        PageElement.objects.get_or_create(page=page_2, content=video_1,
                                          position=0, width=50, height=50,
                                          order=0)
        PageElement.objects.get_or_create(page=page_2, content=video_2,
                                          position=100, width=70, height=70,
                                          order=1)

        self.client = RequestsClient()

    def test_pages(self):
        response = self.client.get('http://testserver/pages')
        data = json.loads(response.text)
        self.assertEqual(data['results'], [
            {'url': 'http://testserver/pages/1/', 'id': 1,
             'title': 'My Page 1'},
            {'url': 'http://testserver/pages/2/', 'id': 2,
             'title': 'My Page 2'}])

    def test_page(self):
        response = self.client.get('http://testserver/pages/1')
        data = json.loads(response.text)
        self.assertDictEqual(
            data, {'title': 'My Page 1', 'id': 1,
                   'elements': [{'height': 100, 'order': 0,
                                 'position': 0, 'width': 100,
                                 'content': {'data': {
                                     'text': 'My Text Content 1',
                                     'type': 'text'},
                                     'counter': 0,
                                     'title': 'My Text 1',
                                     'id': 1}},
                                {'height': 20, 'order': 1,
                                 'position': 100, 'width': 50,
                                 'content': {
                                     'data': {'length': 50,
                                              'source': '/media/audio/'
                                                        'TheHomeBabies-'
                                                        'TheState.mp3',
                                              'type': 'audio'},
                                     'counter': 0,
                                     'title': 'My Audio 1',
                                     'id': 2}},
                                {'height': 200, 'order': 2,
                                 'position': 0, 'width': 200,
                                 'content': {
                                     'data': {'width': 100,
                                              'source': '/media/video/'
                                                        'messi.mp4',
                                              'type': 'video',
                                              'height': 100},
                                     'counter': 0,
                                     'title': 'My Video 1',
                                     'id': 3}}]})

        # check counters increase
        response = self.client.get('http://testserver/pages/1')
        data = json.loads(response.text)
        self.assertEqual([row['content']['counter']
                          for row in data['elements']], [1, 1, 1])

        # call another page and check counters again,
        # first element was called 2 times before,
        # while second element was not called
        response = self.client.get('http://testserver/pages/2')
        data = json.loads(response.text)
        self.assertEqual([row['content']['counter']
                          for row in data['elements']], [2, 0])

        # change order
        response = self.client.get('http://testserver/pages/1?ordering=width')
        data = json.loads(response.text)
        self.assertEqual([row['width']
                          for row in data['elements']], [50, 100, 200])

        # descending order
        response = self.client.get('http://testserver/pages/1?ordering=-width')
        data = json.loads(response.text)
        self.assertEqual([row['width']
                          for row in data['elements']], [200, 100, 50])

        # order by nested obect
        response = self.client.get('http://testserver/pages/1?'
                                   'ordering=content__title')
        data = json.loads(response.text)
        self.assertEqual([row['content']['title'] for row in data['elements']],
                         ['My Audio 1', 'My Text 1', 'My Video 1'])
