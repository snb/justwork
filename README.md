# JustWork API

Test application to manage content pages with text, audio and video content.

## Install

    docker-compose build

## Run

    docker-compose up runserver

## Test

    docker-compose up autotests

## Usage

`http://0.0.0.0/admin/` admin

`http://0.0.0.0/pages/` get all pages

`http://0.0.0.0/pages/1` get exact page

`http://0.0.0.0/pages/3/?ordering=-position` change order

