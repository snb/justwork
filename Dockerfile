FROM ubuntu

# Initialize
RUN mkdir -p /app
WORKDIR /app
ADD requirements.txt /app/

# Setup
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install python3 python3-pip postgresql-client
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

# Prepare
ADD . /app/
